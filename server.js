const express= require('express');
const app= express();

const conexionDB= require('./server/database/conexion')

app.set("view engine", "ejs");

app.get('/', (req , res) => {
    res.render('index');
});


conexionDB();

app.use('/', require('./server/routes/route'))

app.listen(3000, () => {console.log("corriendo")});