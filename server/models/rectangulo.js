const mongoose = require('mongoose');

var rectanguloSchema= new mongoose.Schema({
    puntoX : {
        type : Number
    },
    puntoY : {
        type : Number
    },
    ancho : {
        type : Number
    },
    alto : {
        type : Number
    }
})


const rectangulo= mongoose.model('rectangulo', rectanguloSchema);


module.exports= rectangulo;