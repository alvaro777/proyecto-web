const mongoose= require('mongoose');
const rectangulo = require('./rectangulo');

var claseSchema= new mongoose.Schema({
    titulo : {
        type : String
    },
    atributos : {
        type : Array
    },
    metodos : {
        type : Array
    },
    rectangulo : {
        type : rectangulo
    }
})


const clase= mongoose.model('clase', claseSchema);

module.exports= clase;



















class Clase{
    titulo;
    atributos;
    metodos;
    rectangulo;
}