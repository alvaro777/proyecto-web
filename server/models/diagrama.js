const mongoose= require('mongoose');


var diagramaSchema= new mongoose.Schema({
    nombre : {
        type : String
    },
    listaClases : {
        type : Array
    }
});


const diagrama= mongoose.model('diagrama', diagramaSchema);

module.exports= diagrama;